# 前言
Minetest使用Lua脚本提供模组支持。这本书将会教你学会如何从零开始打造一个属于你自己的MOD。在每一个章节中，我们重点关注每一个API的特定部分，当你学完这本书后，那么你可以自己做一个模组。<br/>
你可以在线读这本书，也可以下载HTML格式(英文原文是这么说的，实际上中文版并没有HTML文件这个选项)。<br/>
反馈和贡献：
如果你发现文中的错误或者提建议，请你通过以下方式告诉我：

1. 新建一个Issue（[Gitlab](https://gitlab.com/rubenwardy/minetest_modding_book/-/issues)/[Gitee](https://gitee.com/ying2002/minetest_modding_book_chs/issues)）<br/>
2. [在讨论区（英文论坛）](https://forum.minetest.net/viewtopic.php?f=14&t=10729)中留言<br/>
3. 发电子邮件给我（877259039@qq.com）<br/>
4. 想添加内容？请移步至[README](https://gitlab.com/rubenwardy/minetest_modding_book/-/blob/master/README.md)（英文原版，目前已迁移至Gitlab上）。<br/>