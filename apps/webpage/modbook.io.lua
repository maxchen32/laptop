dofile(minetest.get_modpath('markdown2formspec').."/init.lua")

local sitename = "modbook.io"
local site = sitename.."/"
local sitepath = minetest.get_modpath('laptop').."/apps/webpage/"..site.."/docs/"

laptop.register_view(sitename , {
	app_info = "Minetest Modding Book 中文版",
	browser_page = true,
	formspec_func = function(app, mtos)
		local formspec = laptop.browser_api.header_formspec_func(app, mtos) ..
				laptop.browser_api.navigator_on_page_func(mtos, sitename, site.."1.md")..
				md2f.md2ff(0.18, 1.5, 15.2, 10.06, sitepath.."/index.md")..
				"background[0,1.2;15,9;laptop_background.png]"
		return formspec
	end,
	receive_fields_func = function(app, mtos, sender, fields)
		laptop.browser_api.header_receive_fields_func(app, mtos, sender, fields)
	end
})

local filelistfull = minetest.get_dir_list(sitepath, false)
local filelist = {}
for _,v in ipairs(filelistfull) do
	local vn = v:match("(.*)%.[^.]+$")
	if not vn:match("[^%d]+") then
		table.insert(filelist, vn)
	end
end

table.sort(filelist,
			function(a,b)
				return tonumber(a) < tonumber(b)
			end)
						
for k,v in ipairs(filelist) do
	print("73:",k,v)
end

for _, filename in ipairs(filelist) do
	laptop.register_view(site..filename..".md", {
		app_info =  filename .. " - modbook",
		browser_page = true,
		formspec_func = function(app, mtos)
			local formspec = laptop.browser_api.header_formspec_func(app, mtos) ..
					laptop.browser_api.navigator_on_page_func(mtos, site.. tonumber(filename)-1 ..".md" , site.. tonumber(filename)+1 ..".md")..
					md2f.md2ff(0.18, 1.5, 15.2, 10.06, sitepath..filename..".md")..
					"background[0,1.2;15,9;laptop_background.png]"
			return formspec
		end,
		receive_fields_func = function(app, mtos, sender, fields)
			laptop.browser_api.header_receive_fields_func(app, mtos, sender, fields)
		end
	})
end

laptop.register_view(site.."1.md", {
	app_info = "1 - modbook",
	browser_page = true,
	formspec_func = function(app, mtos)
		local formspec = laptop.browser_api.header_formspec_func(app, mtos) ..
				laptop.browser_api.navigator_on_page_func(mtos, sitename, site.."2.md")..
				md2f.md2ff(0.18, 1.5, 15.2, 10.06, sitepath.."/1.md")..
				"background[0,1.2;15,9;laptop_background.png]"
		return formspec
	end,
	receive_fields_func = function(app, mtos, sender, fields)
		laptop.browser_api.header_receive_fields_func(app, mtos, sender, fields)
	end
})

local lastfilename = filelist[#filelist]
laptop.register_view(site..lastfilename..".md", {
	app_info =  lastfilename .. " - modbook",
	browser_page = true,
	formspec_func = function(app, mtos)
		local formspec = laptop.browser_api.header_formspec_func(app, mtos) ..
				laptop.browser_api.navigator_on_page_func(mtos, site.. tonumber(lastfilename)-1 ..".md" , sitename)..
				md2f.md2ff(0.18, 1.5, 15.2, 10.06, sitepath..lastfilename..".md")..
				"background[0,1.2;15,9;laptop_background.png]"
		return formspec
	end,
	receive_fields_func = function(app, mtos, sender, fields)
		laptop.browser_api.header_receive_fields_func(app, mtos, sender, fields)
	end
})
